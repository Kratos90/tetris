#pragma once
#include "Figure.h"
class LeftLineFigure :
	public Figure
{
protected:
	void initUp();
	void initDown();
	void initLeft();
	void initRight();
	void fromLeftToDown();
	void fromDownToRight();
	void fromRightToUp();
	void fromUpToLeft();
	void initFigureCoordinates();
public:
	LeftLineFigure();
	~LeftLineFigure();
};

