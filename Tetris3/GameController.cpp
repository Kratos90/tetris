#include "stdafx.h"
#include "GameController.h"

GameController::GameController(int (*tetrisField)[15], int fallFigureSpeed)
{
	gameField = tetrisField;
	nextFigure = false;
	timeoutFallFigure = fallFigureSpeed;
}

void GameController::checkPressedKey()
{
	int elapsedTime = 0, inChar;

	currentFigure;

	while (true) {
		if (_kbhit()) {
			if ((inChar = _gettch()) == 224)
				actionWithPressedKey(_gettch());
		}else if (elapsedTime >= timeoutFallFigure)
			break;

		Sleep(STEP_CHECK_PRESSED_KEY);
		elapsedTime += STEP_CHECK_PRESSED_KEY;
	}
}

void GameController::actionWithPressedKey(int pressedKey)
{
	switch (pressedKey)
	{
		case 72: //up
			changeFigurePosition();
			break;
		case 80: //down
			moveFigureDown();
			break;
		case 75: //left
			moveFigureRightLeft(-1);
			break;
		case 77: //right
			moveFigureRightLeft(1);
			break;
		default:
			break;
	}
}

void GameController::changeFigurePosition()
{
	currentFigure->changePosition();

	if (checkIfOverturnPossible()) {
		drawCell.clearPreviousFigurePosition(currentFigure->tmpFigureCoordinates);
		drawCell.drawFigure(currentFigure->figureCoordinates, currentFigure->color);
	}else
		currentFigure->copyFromTmpCoordinatesToCurrent();
}

void GameController::moveFigureDown()
{
	nextFigure = true;

	drawCell.clearPreviousFigurePosition(currentFigure->figureCoordinates);
	moveUntilFigureDrop(15 - getBiggestFigureY(currentFigure->figureCoordinates));
	drawCell.drawFigure(currentFigure->figureCoordinates, currentFigure->color);
}

void GameController::moveFigureRightLeft(int offset)
{
	if (checkIfPossibleMoveByXCoordinates(offset)) {
		drawCell.clearPreviousFigurePosition(currentFigure->figureCoordinates);
		moveByXCoordinates(offset);
		drawCell.drawFigure(currentFigure->figureCoordinates, currentFigure->color);
	}
}

bool GameController::checkIfPossibleMoveByXCoordinates(int offset)
{
	bool statusPossible = true;
	FigureCells **currentFigureCells = currentFigure->figureCoordinates;
	for (int i = 0; i < 4; i++) {
		if ((currentFigureCells[i]->x + offset) < 0 || (currentFigureCells[i]->x + offset) > 9) {
			statusPossible = false;
			break;
		}else if (gameField[currentFigureCells[i]->x + offset][currentFigureCells[i]->y]) {
			statusPossible = false;
			break;
		}
	}

	return statusPossible;
}

void GameController::moveByXCoordinates(int offset)
{
	for (int i = 0; i < 4; i++)
		currentFigure->figureCoordinates[i]->x += offset;
}

void GameController::moveUntilFigureDrop(int limit)
{
	bool nextYCoordinates = true;
	FigureCells **currentFigureCells = currentFigure->figureCoordinates;

	for (int j = 1; j < limit; j++) {
		for (int i = 0; i < 4; i++) 
			if (gameField[currentFigureCells[i]->x][currentFigureCells[i]->y + 1]) {
				nextYCoordinates = false;
				break;
			}

		if (!nextYCoordinates)
			break;
		else
			drawCell.increaseYCoordinates(currentFigureCells);
	}
}

int GameController::getBiggestFigureY(FigureCells **currentFigureCells)
{
	int biggestFigureY = 0;
	for (int i = 0; i < 4; i++) 
		if (currentFigureCells[i]->y > biggestFigureY) 
			biggestFigureY = currentFigureCells[i]->y;

	return biggestFigureY;
}

bool GameController::checkIfOverturnPossible()
{
	int x, y;
	bool status = true;
	for (int i = 0; i < 4; i++) {
		x = currentFigure->figureCoordinates[i]->x;
		y = currentFigure->figureCoordinates[i]->y;

		if (gameField[x][y]) {
			status = false;
			break;
		}
	}

	return status;
}

GameController::~GameController()
{
}