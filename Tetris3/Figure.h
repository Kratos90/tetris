#pragma once
class Figure
{
protected:
	int position;
	int quantityPositions;
	int x, y;
	void initDefaultCoordinates();
	void generatePosition();
	void createEmptyFigureCells();
	void getCoordinatesPosition();
	void incrementPosition();
	bool isChangePosition;
	//void generateRandomColor();
	virtual void initFigureCoordinates();
public:
	int color;

	FigureCells *figureCoordinates[4];
	FigureCells *tmpFigureCoordinates[4];

	Figure();

	void copyCurrentCoordinatesToTmp();
	void copyFromTmpCoordinatesToCurrent();
	void changePosition();

	~Figure();
};

