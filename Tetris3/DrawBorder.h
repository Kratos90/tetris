#pragma once
class DrawBorder
{
private:
	HANDLE handel = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD coord;
	void SetupAngle();
	void drawElement(int x, int y, char elementCode);
	void drawHorizontalBorder(int x, int y);
	void drawVerticalBorder(int x, int y);
public:
	DrawBorder();
	void init();
	~DrawBorder();
};

