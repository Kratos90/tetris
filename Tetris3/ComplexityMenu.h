#pragma once
#include "SubMenu.h"

class ComplexityMenu :
	public SubMenu
{
protected:
	void initElements();
public:
	ComplexityMenu(Menu *menuController, int countMenuElements);
	void actionWithMenuElements();
	~ComplexityMenu();
};

