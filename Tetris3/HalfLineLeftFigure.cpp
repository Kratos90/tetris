#include "stdafx.h"
#include "HalfLineLeftFigure.h"

HalfLineLeftFigure::HalfLineLeftFigure()
{
	color = 13; //purple
	quantityPositions = 2;
	generatePosition();
	initFigureCoordinates();
}

void HalfLineLeftFigure::initHorizontalLine()
{
	if (x == 0 && y == 0)
		x = 5;
	else if (isChangePosition) 
		y--;

	int i;

	for (i = 0; i < 2; i++, x++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

	for (x -= 3, y++; i < 4; i++, x++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

}

void HalfLineLeftFigure::initVerticalLine()
{
	if (x == 0 && y == 0)
		x = 4;
	else if (isChangePosition) {
		y -= 2;
		x++;
	}

	int i;

	for (i = 0; i < 2; i++) {
		figureCoordinates[i]->x = x;
		y += i;
		figureCoordinates[i]->y = y;
	}

	for (x++; i < 4; i++, y++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void HalfLineLeftFigure::initFigureCoordinates()
{
	switch (position)
	{
	case 1:
		initHorizontalLine();
		break;
	case 2:
		initVerticalLine();
		break;
	default:
		break;
	}
}

HalfLineLeftFigure::~HalfLineLeftFigure()
{
}
