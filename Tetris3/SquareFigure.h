#pragma once
#include "Figure.h"

class SquareFigure :
	public Figure
{
protected:
	void incrementPosition();
	void getCoordinatesPosition();
	void initFigureCoordinates();
public:
	SquareFigure();
	void changePosition();
	~SquareFigure();
};

