#include "stdafx.h"
#include "Figure.h"

Figure::Figure()
{
	isChangePosition = false;
	//generateRandomColor();
	createEmptyFigureCells();
	initDefaultCoordinates();
}

void Figure::initDefaultCoordinates() {
	x = y = 0;
}

void Figure::generatePosition() {
	position = rand() % quantityPositions + 1;
}

void Figure::createEmptyFigureCells() {
	for (int y = 0, i = 0; y < 4; y++, i++) {
		figureCoordinates[i] = new FigureCells();
		tmpFigureCoordinates[i] = new FigureCells();

		tmpFigureCoordinates[i]->x = figureCoordinates[i]->x = 0;
		tmpFigureCoordinates[i]->y = figureCoordinates[i]->y = 0;
	}
}

void Figure::copyFromTmpCoordinatesToCurrent() {
	for (int i = 0; i < 4; i++) {
		figureCoordinates[i]->x = tmpFigureCoordinates[i]->x;
		figureCoordinates[i]->y = tmpFigureCoordinates[i]->y;
	}
}

void Figure::copyCurrentCoordinatesToTmp() {
	for (int i = 0; i < 4; i++) {
		tmpFigureCoordinates[i]->x = figureCoordinates[i]->x;
		tmpFigureCoordinates[i]->y = figureCoordinates[i]->y;
	}
}

void Figure::getCoordinatesPosition() {
	initDefaultCoordinates();
	x = figureCoordinates[0]->x;

	for (int i = 0; i < 4; i++) {
		if (figureCoordinates[i]->y > y)
			y = figureCoordinates[i]->y;

		if (figureCoordinates[i]->x < x)
			x = figureCoordinates[i]->x;
	}

}

void Figure::incrementPosition() {
	position++;

	if (position > quantityPositions)
		position = 1;
}

void Figure::changePosition() {
	isChangePosition = true;
	incrementPosition();
	getCoordinatesPosition();
	copyCurrentCoordinatesToTmp();
	initFigureCoordinates();
}

//void Figure::generateRandomColor()
//{
//	color = rand() % 15 + 1;
//}

void Figure::initFigureCoordinates() {}

Figure::~Figure()
{
}
