#include "stdafx.h"
#include "PlayTetris.h"


PlayTetris::PlayTetris()
{
	countFigures = 6;
}

void PlayTetris::start(int timeoutFallFigure)
{
	init();
	tetrisController = new GameController(tetrisField, timeoutFallFigure);

	while (generateFigure()) {}
	tetrisScore.gameOver();

	delete tetrisController;
}

void PlayTetris::init()
{
	tetrisScore.setScore(0);
	tetrisScore.writeCurrentScore();
	TetrisBorder.init();
	initTetrisFieldZeros();
}

void PlayTetris::initTetrisFieldZeros()
{
	for (int x = 0; x < 10; x++)
		for (int y = 0; y < 15; y++)
			tetrisField[x][y] = 0;
}

Figure * PlayTetris::generateRandomFigure()
{
	Figure *newFigure;

	switch (getRandomIndex())
	{
	case 1:
		newFigure = new LineFigure;
		break;
	case 2:
		newFigure = new SquareFigure;
		break;
	case 3:
		newFigure = new TriangleFigure;
		break;
	case 4:
		newFigure = new LeftLineFigure;
		break;
	case 5:
		newFigure = new RightLineFigure;
		break;
	case 6:
		newFigure = new HalfLineRightFigure;
		break;
	case 7:
		newFigure = new HalfLineLeftFigure;
		break;
	default:
		newFigure = new SquareFigure;
		break;
	}
	
	return newFigure;
}

int PlayTetris::getRandomIndex()
{
	return rand() % countFigures + 1;
}

bool PlayTetris::generateFigure()
{
	tetrisController->currentFigure = currentFigure = generateRandomFigure();
	currentFigureCells = currentFigure->figureCoordinates;
	
	bool showFigure = checkIfFreePlaceExist(currentFigureCells);

	//Drop figure
	while (showFigure) {

		simpleDrawCell.drawFigure(currentFigureCells, currentFigure->color);

		tetrisController->checkPressedKey();

		if (tetrisController->nextFigure || checkIfNeedStopFigure()) {
			saveFigureToTetrisField();
			checkFullLines();
			tetrisController->nextFigure = false;
			redrawGameField();
			break;
		}

		simpleDrawCell.clearPreviousFigurePosition(currentFigureCells);
		simpleDrawCell.increaseYCoordinates(currentFigureCells);
	}

	delete currentFigure;

	return showFigure;
}

bool PlayTetris::checkIfFreePlaceExist(FigureCells ** currentFigureCells)
{
	bool showFigure = true;

	for (int i = 0; i < 4; i++) 
		if (tetrisField[currentFigureCells[i]->x][currentFigureCells[i]->y])
			showFigure = false;

	return showFigure;
}

void PlayTetris::checkFullLines()
{
	int i, indexYFullLineCoordinate = 0, countFullLines = 0;
	int yCoordinateWithFullLines[15];
	//TODO get height y coordinate with some figure part, and replace j value
	for (int j = 0; j < 15; j++) {
		for (i = 0; i < 10; i++) {
			if (!tetrisField[i][j])
				break;
		}

		if (i == 10) {
			yCoordinateWithFullLines[indexYFullLineCoordinate] = j;
			indexYFullLineCoordinate++;
			countFullLines++;
		}

	}

	if (countFullLines) {
		tetrisScore.calculateScoreAfterClearFullLine(countFullLines);
		tetrisScore.writeCurrentScore();
		clearLine(yCoordinateWithFullLines);
		redrawGameField();
	}
}

void PlayTetris::clearLine(int * yCoordinateWithFullLines)
{
	int y = 0;
	int heightY = getHeightPositionWithSomeFigurePart();

	for (int k = 0; k < 15; k++)
		if ((y = *(yCoordinateWithFullLines + k)) && y >= 1)
			for (; heightY <= y; y--)
				for (int x = 0; x < 10; x++) 
					tetrisField[x][y] = tetrisField[x][y - 1];
		else
			break;
}

int PlayTetris::getHeightPositionWithSomeFigurePart()
{
	int y = 0;
	bool found = false;

	for (y = 0; y < 15; y++) {
		for (int x = 0; x < 10; x++)
			if (tetrisField[x][y]) {
				found = true;
				break;
			}

		if (found)
			break;
	}

	return y;
}

void PlayTetris::redrawGameField()
{
	for (int x = 0; x < 10; x++)
		for (int y = 0; y < 15; y++) {
			simpleDrawCell.setColorOutputChar(tetrisField[x][y]);
			if (tetrisField[x][y])
				simpleDrawCell.drawByCoordinates(x, y);
			else
				simpleDrawCell.clearByCoordinates(x, y);
		}
			
}

bool PlayTetris::checkIfNeedStopFigure()
{
	int needBreak = false, biggestY = 0;

	for (int i = 0; i < 4; i++)
		if (tetrisField[currentFigureCells[i]->x][currentFigureCells[i]->y + 1])
			needBreak = true;
		else if (currentFigureCells[i]->y > biggestY)
			biggestY = currentFigureCells[i]->y;

	if (biggestY == (15 - 1))
		needBreak = true;

	return needBreak;
}

void PlayTetris::saveFigureToTetrisField()
{
	for (int i = 0; i < 4; i++)
		tetrisField[currentFigureCells[i]->x][currentFigureCells[i]->y] = currentFigure->color;
}

PlayTetris::~PlayTetris()
{
}