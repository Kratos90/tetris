// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <conio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <stdlib.h>
#include <string.h>

using namespace std;

struct FigureCells {
	int x;
	int y;
};

#define WIDTH 10
#define HEIGHT 15
#define TIMEOUT_FALL_FIGURE 250
#define STEP_CHECK_PRESSED_KEY 50

#define EASY_COMPLEXITY 500
#define NORMAL_COMPLEXITY 350
#define HARD_COMPLEXITY 200

#define COUNT_ELEMENTS_TOP_MENU 4
#define COUNT_ELEMENTS_COMPLEXITY 3


// TODO: reference additional headers your program requires here
