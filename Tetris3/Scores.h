#pragma once
#include "DrawCell.h"

class Scores
{
protected:
	DrawCell drawCell;
	int xCoordinateForScore;
	int yCoordinateForScore;
	int score;
	void checkIfNeedSaveScoreLikeABestScore();
	int getBestScore(fstream *bestScoreFile);
	void compareNewScoreWithBest(fstream *bestScoreFile, int bestScore);
	int openFileWithBestScore();
public:
	Scores();
	void writeCurrentScore();
	void calculateScoreAfterClearFullLine(int countFullLines);
	int getScore();
	void setScore(int newScore);
	void gameOver();
	void showBestScore();
	~Scores();
};

