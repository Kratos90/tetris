#pragma once
#include "Figure.h"
class RightLineFigure :
	public Figure
{
protected:
	void initUp();
	void initDown();
	void initLeft();
	void initRight();
	void fromRightToUp();
	void fromUpToLeft();
	void fromLeftToDown();
	void fromDownToRight();
	void initFigureCoordinates();
public:
	RightLineFigure();
	~RightLineFigure();
};

