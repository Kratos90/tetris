#include "stdafx.h"
#include "Menu.h"

Menu::Menu() {
	timeoutFallFigure = NORMAL_COMPLEXITY;

	topMenu = new TopLevelMenu(this, COUNT_ELEMENTS_TOP_MENU);
	complexityMenu = new ComplexityMenu(this, COUNT_ELEMENTS_COMPLEXITY);

	activeSubMenu = topMenu;

	init(topMenu->menuElements, topMenu->quantityElements);
}

void Menu::init(string *pmenu_elms, int quntity_menu_elements)
{
	option_menu = pmenu_elms;
	this->menu_quantity = quntity_menu_elements;

	coord.Y = 0;
	coord.X = 0;

	getLengthLongestElement();
	showMenuStatus = true;

	startShowMenu();
}

void Menu::getLengthLongestElement() {
	int max_length = (*option_menu).size();

	for (int ind = 1; ind < menu_quantity; ind++) {
		if (option_menu[ind].size() > max_length) {
			max_length = option_menu[ind].size();
		}
	}

	right_arrow_pos = 3 + max_length + 2;
}

void Menu::startShowMenu() {
	showMenu();
	drawElementMenu(233);

	while (showMenuStatus) {
		key = _getch();

		if (key == 72 && coord.Y > 0) {
			moveArrow(-1);
		}
		else if (key == 80 && coord.Y < (menu_quantity - 1)) {
			moveArrow(1);
		}
		else if (key == 13) {
			//actionWithMenuElement();
			activeSubMenu->actionWithMenuElements();
		}
		else if (key == 27) {
			break;
		}
	}
}

void Menu::showMenu() {
	clearConsoleAndSetColorBlack();

	for (int ind = 0; ind < menu_quantity; ind++) {
		cout << "   " << option_menu[ind] << endl;
	}

	coord.Y = 0;

	drowArrowPosition(0, 12, "->");
	drowArrowPosition(right_arrow_pos, 12, "<-");
}

void Menu::clearConsoleAndSetColorBlack() {
	system("CLS");
	system("color 0f");
}

void Menu::drowArrowPosition(int x_pos, int color_value, string value) {
	coord.X = x_pos;

	SetConsoleCursorPosition(handel, coord);
	SetConsoleTextAttribute(handel, color_value);

	cout << value;
}

void Menu::moveArrow(int y_pos) {
	drowArrowPosition(0, 0, "  ");
	drowArrowPosition(right_arrow_pos, 0, "  ");

	drawElementMenu(15);
	coord.Y += y_pos;
	drawElementMenu(233);

	drowArrowPosition(0, 12, "->");
	drowArrowPosition(right_arrow_pos, 12, "<-");
}

void Menu::drawElementMenu(int color_value) {
	coord.X = 3;

	SetConsoleCursorPosition(handel, coord);
	SetConsoleTextAttribute(handel, color_value);

	cout << option_menu[coord.Y];
}

void Menu::actionWithMenuElement() {
	clearConsoleAndSetColorBlack();

	switch (coord.Y) {
	case 0:
		startTetris();
		break;
	case 1:
		showBestScore();
		break;
	case 2:
		showMenuStatus = false;
		break;
	default:
		break;
	}
}

void Menu::showBestScore()
{
	tetrisScore.showBestScore();
	showMenu();
	drawElementMenu(233);
}

void Menu::startTetris()
{
	tetris.start(timeoutFallFigure);

	Sleep(2500);
	showMenu();
	drawElementMenu(233);
}

string Menu::getMsgBySelectedMenuOption() {
	return option_menu[coord.Y];
}

Menu::~Menu()
{
}
