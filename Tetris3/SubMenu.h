#pragma once
//#include "Menu.h"
class Menu;
class SubMenu
{
protected:
	virtual void initElements() = 0;
public:
	Menu *menu;
	const int quantityElements;
	string *menuElements;

	SubMenu(const int countElements);
	virtual void actionWithMenuElements() = 0;
	~SubMenu();
};

