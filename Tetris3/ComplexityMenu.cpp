#include "stdafx.h"
#include "ComplexityMenu.h"
#include "Menu.h"

ComplexityMenu::ComplexityMenu(Menu *menuController, int countMenuElements):SubMenu(countMenuElements)
{
	menu = menuController;
	initElements();
}

void ComplexityMenu::initElements()
{
	int index = 0;
	menuElements = new string[quantityElements];

	menuElements[index++] = "Easy";
	menuElements[index++] = "Normal";
	menuElements[index++] = "Hard";
}

void ComplexityMenu::actionWithMenuElements()
{
	menu->clearConsoleAndSetColorBlack();

	switch (menu->coord.Y) {
		case 0:
			menu->timeoutFallFigure = EASY_COMPLEXITY;
			break;
		case 1:
			menu->timeoutFallFigure = NORMAL_COMPLEXITY;
			break;
		case 2:
			menu->timeoutFallFigure = HARD_COMPLEXITY;
			break;
		default:
			break;
	}

	menu->activeSubMenu = menu->topMenu;
	menu->init(menu->topMenu->menuElements, menu->topMenu->quantityElements);
}


ComplexityMenu::~ComplexityMenu()
{
}
