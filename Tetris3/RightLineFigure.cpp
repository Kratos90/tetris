#include "stdafx.h"
#include "RightLineFigure.h"


RightLineFigure::RightLineFigure()
{
	color = 15; //white
	quantityPositions = 4;
	generatePosition();
	initFigureCoordinates();
}

void RightLineFigure::initUp()
{
	if (x == 0 && y == 0) {
		x = 4;
		y = 1;
	}else if (y == 0)
		y = 1;
	else if (isChangePosition)
		fromRightToUp();

	int i = 0, limit = x + 3;

	for (i = 0; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

	figureCoordinates[i]->x = x - 1;
	figureCoordinates[i]->y = y - 1;
}

void RightLineFigure::initDown()
{
	if (x == 0 && y == 0) {
		x = 4;
		y = 1;
	}
	else if (y == 0)
		y = 1;
	else if (isChangePosition)
		fromLeftToDown();

	int i = 0, limit = x + 3;

	figureCoordinates[i]->x = x;
	figureCoordinates[i]->y = y + 1;

	for (i = 1; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void RightLineFigure::initLeft()
{
	if (x == 0 && y == 0)
		x = 5;
	else if (isChangePosition)
		fromUpToLeft();

	int i = 0, limit = y + 3;

	figureCoordinates[i]->x = x - 1;
	figureCoordinates[i]->y = y;

	for (i = 1; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void RightLineFigure::initRight()
{
	if (x == 0 && y == 0)
		x = 4;
	else if (isChangePosition)
		fromDownToRight();

	int i = 0, limit = y + 3;

	figureCoordinates[i]->x = x + 1;
	figureCoordinates[i]->y = y + 2;

	for (i = 1; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void RightLineFigure::fromDownToRight()
{
	y -= 2;
}

void RightLineFigure::fromRightToUp()
{
}

void RightLineFigure::fromUpToLeft()
{
	x++;
	y -= 2;
}

void RightLineFigure::fromLeftToDown()
{
	y--;
}

void RightLineFigure::initFigureCoordinates()
{
	switch (position)
	{
	case 1:
		initRight();
		break;
	case 2:
		initUp();
		break;
	case 3:
		initLeft();
		break;
	case 4:
		initDown();
		break;
	default:
		break;
	}
}

RightLineFigure::~RightLineFigure()
{
}