#include "stdafx.h"
#include "TopLevelMenu.h"
#include "Menu.h"

TopLevelMenu::TopLevelMenu(Menu *menuController, int countMenuElements):SubMenu(countMenuElements)
{
	menu = menuController;
	initElements();
}

void TopLevelMenu::initElements()
{
	int index = 0;
	menuElements = new string[quantityElements];

	menuElements[index++] = "New Game";
	menuElements[index++] = "Complexity";
	menuElements[index++] = "Best Score";
	menuElements[index++] = "Exit";
}

void TopLevelMenu::actionWithMenuElements()
{
	menu->clearConsoleAndSetColorBlack();
	
	switch (menu->coord.Y) {
		case 0:
			menu->startTetris();
			break;
		case 1:
			menu->activeSubMenu = menu->complexityMenu;
			menu->init(menu->complexityMenu->menuElements, menu->complexityMenu->quantityElements);
			break;
		case 2:
			menu->showBestScore();
			break;
		case 3:
			menu->showMenuStatus = false;
			break;
		default:
			break;
	}
}


TopLevelMenu::~TopLevelMenu()
{
}
