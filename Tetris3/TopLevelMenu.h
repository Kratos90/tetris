#pragma once
#include "SubMenu.h"

class TopLevelMenu :
	public SubMenu
{
protected:
	void initElements();
public:
	TopLevelMenu(Menu *menuController, int countMenuElements);
	void actionWithMenuElements();
	~TopLevelMenu();
};

