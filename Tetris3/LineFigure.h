#pragma once
#include "Figure.h"
class LineFigure :
	public Figure
{
protected:
	void initHorizontalLine();
	void initVerticalLine();
	void initFigureCoordinates();
public:
	LineFigure();
	~LineFigure();
};

