#include "stdafx.h"
#include "HalfLineRightFigure.h"


HalfLineRightFigure::HalfLineRightFigure()
{
	color = 11; //light blue
	quantityPositions = 2;
	generatePosition();
	initFigureCoordinates();
}

void HalfLineRightFigure::initHorizontalLine()
{
	if (x == 0 && y == 0)
		x = 4;
	else if (isChangePosition) {
		x--;
		y--;
	}

	int i;

	for (i = 0; i < 2; i++, x++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
	
	x--;
	y++;

	for (; i < 4; i++, x++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

}

void HalfLineRightFigure::initVerticalLine()
{
	if (x == 0 && y == 0) 
		x = 5;
	else if (isChangePosition) {
		y -= 2;
		x += 2;
	}

	int i;

	for (i = 0; i < 2; i++) {
		figureCoordinates[i]->x = x;
		y += i;
		figureCoordinates[i]->y = y;
	}

	x--;

	for (; i < 4; i++, y++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void HalfLineRightFigure::initFigureCoordinates()
{
	switch (position)
	{
	case 1:
		initHorizontalLine();
		break;
	case 2:
		initVerticalLine();
		break;
	default:
		break;
	}
}

HalfLineRightFigure::~HalfLineRightFigure()
{
}