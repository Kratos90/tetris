#pragma once
#include "Figure.h"
class HalfLineRightFigure :
	public Figure
{
protected:
	void initHorizontalLine();
	void initVerticalLine();
	void initFigureCoordinates();
public:
	HalfLineRightFigure();
	~HalfLineRightFigure();
};

