#pragma once
#include "DrawCell.h"
#include "DrawBorder.h"
#include "Scores.h"
#include "GameController.h"
#include "Figure.h"
#include "SquareFigure.h"
#include "LineFigure.h"
#include "TriangleFigure.h"
#include "LeftLineFigure.h"
#include "RightLineFigure.h"
#include "HalfLineRightFigure.h"
#include "HalfLineLeftFigure.h"

class PlayTetris
{
private:
	int tetrisField[10][15];
	DrawBorder TetrisBorder;
	DrawCell simpleDrawCell;
	int countFigures;
	Scores tetrisScore;
	Figure *currentFigure;
	FigureCells **currentFigureCells;
	GameController *tetrisController;
	bool nextFigure = false;

	void init();
	void initTetrisFieldZeros();
	Figure *generateRandomFigure();
	int getRandomIndex();
	bool generateFigure();
	bool checkIfFreePlaceExist(FigureCells **currentFigureCells);
	void checkFullLines();
	void clearLine(int *yCoordinateWithFullLines);
	int getHeightPositionWithSomeFigurePart();
	void redrawGameField();
	bool checkIfNeedStopFigure();
	void saveFigureToTetrisField();
public:
	PlayTetris();
	void start(int timeoutFallFigure);
	~PlayTetris();
};

