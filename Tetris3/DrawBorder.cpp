#include "stdafx.h"
#include "DrawBorder.h"

DrawBorder::DrawBorder()
{
}

void DrawBorder::init()
{
	SetupAngle();

	drawHorizontalBorder(2, 1);
	drawHorizontalBorder(2, 32);

	drawVerticalBorder(1, 2);
	drawVerticalBorder(22, 2);
}

void DrawBorder::SetupAngle()
{
	drawElement(1, 1, 201);
	drawElement(1, 32, 200);
	drawElement(22, 1, 187);
	drawElement(22, 32, 188);
}

void DrawBorder::drawElement(int x, int y, char elementCode) {
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(handel, coord);

	cout << elementCode;
}

void DrawBorder::drawHorizontalBorder(int x, int y)
{
	for (; x < 22; x++) 
		drawElement(x, y, 205);
}

void DrawBorder::drawVerticalBorder(int x, int y)
{
	for (; y < 32; y++)
		drawElement(x, y, 186);
}

DrawBorder::~DrawBorder()
{
}
