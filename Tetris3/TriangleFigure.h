#pragma once
#include "Figure.h"

class TriangleFigure :
	public Figure
{
protected:
	void initUp();
	void initDown();
	void initLeft();
	void initRight();
	void fromDownToRight();
	void fromRightToUp();
	void fromUpToLeft();
	void fromLeftToDown();
	void initFigureCoordinates();
public:
	TriangleFigure();
	~TriangleFigure();
};

