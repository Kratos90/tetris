#include "stdafx.h"
#include "LoadSave.h"


LoadSave::LoadSave()
{
	fstream bestScoreFile;
	bestScoreFile.open("Saves.txt", ios::in | ios::out | ios::binary);
	int tetrisField[10][15] = { 0 };
	char line[100];
	bestScoreFile >> line;
	char everyLine[4];

	for (int i = 0, index = 0, x = 0; i < 100; i++, index++) {
		if (line[i] == ';') {
			char bin[16];
			int dec = strtol(everyLine, NULL, 16);
			_itoa_s(dec, bin, 2);

			int countDigits = 0;
			for (int k = 0; k < 15; k++, countDigits++) {
				if (bin[k] == '\0') {
					break;
				}
			}

			char newbin[16];
			if (countDigits < 15) {
				int countZeros = 15 - countDigits;
				int index = 0;
				for (int k = 0; k < 15; k++) {
					if (k < countZeros) {
						newbin[k] = '0';
					}
					else {
						newbin[k] = bin[index];
						index++;
					}
				}

				cout << newbin << " : " << countDigits << endl;
			}
			else
				cout << bin << " : " << countDigits << endl;


			index = -1;
			continue;
		}

		everyLine[index] = line[i];
	}

	//char newBestScore[20];
	//char a[4] = { '7', '9', '9', '9' };
	//int score = strtol(a, NULL, 16);
	//_itoa_s(score, newBestScore, 2);

	//cout << newBestScore << endl;

	bestScoreFile.close();
}


LoadSave::~LoadSave()
{
}
