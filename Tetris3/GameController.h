#include "stdafx.h"
#include "Figure.h"
#include "DrawCell.h"
#pragma once
class GameController
{
protected:
	DrawCell drawCell;
	int timeoutFallFigure;
	int (*gameField)[15];
	void actionWithPressedKey(int pressedKey);
	void changeFigurePosition();
	void moveFigureRightLeft(int offset);
	bool checkIfPossibleMoveByXCoordinates(int offset);
	void moveByXCoordinates(int offset);
	void moveUntilFigureDrop(int limit);
	int getBiggestFigureY(FigureCells ** currentFigureCells);
	bool checkIfOverturnPossible();
public:
	Figure *currentFigure;
	bool nextFigure;
	GameController(int (*tetrisField)[15], int fallFigureSpeed);
	void moveFigureDown();
	void checkPressedKey();
	~GameController();
};

