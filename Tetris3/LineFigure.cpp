#include "stdafx.h"
#include "LineFigure.h"
#include "Figure.h"

LineFigure::LineFigure()
{
	color = 9; //blue
	quantityPositions = 2;
	generatePosition();
	initFigureCoordinates();
}

void LineFigure::initHorizontalLine() {
	if (x == 0 && y == 0) 
		x = 3;

	int limit = x + 4;

	for (int i = 0; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void LineFigure::initVerticalLine() {
	if (x == 0 && y == 0) 
		x = 4;
	else if (y >= 4) 
		y = y - 3;

	int limit = y + 4;

	for (int i = 0; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void LineFigure::initFigureCoordinates() {
	switch (position)
	{
	case 1:
		initHorizontalLine();
		break;
	case 2:
		initVerticalLine();
		break;
	default:
		break;
	}
}

LineFigure::~LineFigure()
{
}
