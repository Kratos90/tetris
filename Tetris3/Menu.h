#pragma once
#include "stdafx.h"
#include "PlayTetris.h"
#include "TopLevelMenu.h"
#include "ComplexityMenu.h"

class Menu
{
protected:
	HANDLE handel = GetStdHandle(STD_OUTPUT_HANDLE);
	int key, right_arrow_pos, menu_quantity;
	string *option_menu;
	Scores tetrisScore;
	PlayTetris tetris;

	void getLengthLongestElement();
	void showMenu();
	void drowArrowPosition(int x_pos, int color_value, string value);
	void moveArrow(int y_pos);
	void drawElementMenu(int);
	void actionWithMenuElement();
	string getMsgBySelectedMenuOption();
	void startShowMenu();

public:
	COORD coord;
	bool showMenuStatus;
	SubMenu *activeSubMenu;
	TopLevelMenu *topMenu;
	ComplexityMenu *complexityMenu;
	int timeoutFallFigure;

	Menu();
	void init(string *pmenu_elms, int quntity_menu_elements);
	void clearConsoleAndSetColorBlack();
	void showBestScore();
	void startTetris();
	~Menu();
};

