#pragma once
#include "Figure.h"
class HalfLineLeftFigure :
	public Figure
{
protected:
	void initHorizontalLine();
	void initVerticalLine();
	void initFigureCoordinates();
public:
	HalfLineLeftFigure();
	~HalfLineLeftFigure();
};

