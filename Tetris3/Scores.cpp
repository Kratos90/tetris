#include "stdafx.h"
#include "Scores.h"

#define BESTSCORE "BestScore.txt"

Scores::Scores()
{
	score = 0;
	xCoordinateForScore = 12;
	yCoordinateForScore = 3;
}

void Scores::writeCurrentScore()
{
	char scoreChar[10];
	char scoreStringValue[30] = "Score : ";

	_itoa_s(score, scoreChar, 10);

	strcat_s(scoreStringValue, scoreChar);
	
	drawCell.writeStringByCoordinates(scoreStringValue, xCoordinateForScore, yCoordinateForScore);
}

void Scores::calculateScoreAfterClearFullLine(int countFullLines)
{
	if (countFullLines > 1)
		score += countFullLines * 2;
	else
		score += countFullLines;
}

int Scores::getScore()
{
	return score;
}

void Scores::setScore(int newScore)
{
	score = newScore;
}

void Scores::gameOver()
{
	drawCell.writeStringByCoordinates("GAME OVER", xCoordinateForScore, yCoordinateForScore - 1);
	checkIfNeedSaveScoreLikeABestScore();
}

void Scores::checkIfNeedSaveScoreLikeABestScore()
{
	fstream bestScoreFile;
	bestScoreFile.open(BESTSCORE, ios::in | ios::out | ios::binary);
	int bestScore = -1;
	
	if (!bestScoreFile.is_open()) {
		bestScoreFile.open(BESTSCORE, ios::out);
		bestScore = 0;
	}

	compareNewScoreWithBest(&bestScoreFile, bestScore == 0 ? 0 : getBestScore(&bestScoreFile));

	bestScoreFile.close();
}

void Scores::showBestScore()
{
	cout << "Best score is : " << openFileWithBestScore();

	Sleep(2000);
}

int Scores::openFileWithBestScore() {
	fstream bestScoreFile;
	bestScoreFile.open(BESTSCORE, ios::in | ios::out | ios::binary);

	int bestScore = 0;

	if (bestScoreFile.is_open())
		bestScore = getBestScore(&bestScoreFile);

	bestScoreFile.close();

	return bestScore;
}

int Scores::getBestScore(fstream *bestScoreFile)
{
	char line[20];
	*bestScoreFile >> line;
	return atoi(line);
}

void Scores::compareNewScoreWithBest(fstream *bestScoreFile, int bestScore) 
{
	if (score > bestScore) {
		char newBestScore[20];
		_itoa_s(score, newBestScore, 10);

		bestScoreFile->clear();
		bestScoreFile->seekg(0);
		*bestScoreFile << newBestScore;
	}
}

Scores::~Scores()
{
}
