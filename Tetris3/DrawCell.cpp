#include "stdafx.h"
#include "DrawCell.h"

DrawCell::DrawCell()
{
	this->elementCode = 219;
	SetConsoleTextAttribute(handel, 8);
}

void DrawCell::drawByCoordinates(int x, int y)
{
	convertCoordinates(x, y);
	drawCellByConvertCoordinates();
}


void DrawCell::convertCoordinates(int x, int y)
{
	xConverted = (x + 1) * 2;
	yConverted = (y + 1) * 2;
}

void DrawCell::drawCellByConvertCoordinates()
{
	coord.X = xConverted;
	for (coord.Y = yConverted; coord.Y < yConverted + 2; coord.Y++) {
		printTwoChars();
	}
}

void DrawCell::clearByCoordinates(int x, int y)
{
	convertCoordinates(x, y);
	int tmpSaveElement = elementCode;
	elementCode = 32;

	drawCellByConvertCoordinates();

	elementCode = tmpSaveElement;
}

void DrawCell::printTwoChars()
{
	SetConsoleCursorPosition(handel, coord);

	cout << (char)elementCode;
	cout << (char)elementCode;
}

void DrawCell::writeStringByCoordinates(char *string, int x, int y)
{
	convertCoordinates(x, y);
	coord.X = xConverted;
	coord.Y = yConverted;

	SetConsoleCursorPosition(handel, coord);

	cout << string;
}

void DrawCell::clearPreviousFigurePosition(FigureCells ** currentFigureCells)
{
	setColorOutputChar(0); //set black color

	for (int i = 0; i < 4; i++) 
		clearByCoordinates(currentFigureCells[i]->x, currentFigureCells[i]->y);
}

void DrawCell::drawFigure(FigureCells ** currentFigureCells, int color) {
	setColorOutputChar(color);

	for (int i = 0; i < 4; i++)
		drawByCoordinates(currentFigureCells[i]->x, currentFigureCells[i]->y);
}

void DrawCell::increaseYCoordinates(FigureCells ** currentFigureCells)
{
	for (int i = 0; i < 4; i++)
		currentFigureCells[i]->y++;
}

void DrawCell::setColorOutputChar(int color)
{
	SetConsoleTextAttribute(handel, color);
}

DrawCell::~DrawCell()
{
}
