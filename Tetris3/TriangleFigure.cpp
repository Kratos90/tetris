#include "stdafx.h"
#include "TriangleFigure.h"

TriangleFigure::TriangleFigure()
{
	color = 12; //red
	quantityPositions = 4;
	generatePosition();
	initFigureCoordinates();
}


void TriangleFigure::initUp()
{
	if (x == 0 && y == 0) {
		x = 4;
		y = 1;
	}else if (y == 0)
		y = 1;
	else if (isChangePosition)
		fromRightToUp();

	int i = 0, limit = x + 3;

	figureCoordinates[i]->x = x + 1;
	figureCoordinates[i]->y = y - 1;

	for (i = 1; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

}

void TriangleFigure::initDown()
{
	if (x == 0 && y == 0)
		x = 4;
	else if (isChangePosition)
		fromLeftToDown();

	int i, limit = x + 3, highter = x + 1;

	for (i = 0; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

	figureCoordinates[i]->x = highter;
	figureCoordinates[i]->y = y + 1;
}

void TriangleFigure::initLeft()
{
	if (x == 0 && y == 0)
		x = 5;
	else if (isChangePosition)
		fromUpToLeft();

	int i = 0 , limit = y + 3;

	figureCoordinates[i]->x = x - 1;
	figureCoordinates[i]->y = y + 1;

	for (i = 1; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

}

void TriangleFigure::initRight()
{
	if (x == 0 && y == 0)
		x = 5;
	else if (isChangePosition)
		fromDownToRight();

	int i = 0, limit = y + 3;

	figureCoordinates[i]->x = x + 1;
	figureCoordinates[i]->y = y + 1;

	for (i = 1; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void TriangleFigure::fromDownToRight()
{
	x++;
	y -= 2;
}

void TriangleFigure::fromRightToUp()
{
	x--;
}

void TriangleFigure::fromUpToLeft()
{
	x++;
	y -= 2;
}

void TriangleFigure::fromLeftToDown()
{
	y--;
}

void TriangleFigure::initFigureCoordinates()
{
	switch (position)
	{
	case 1:
		initDown();
		break;
	case 2:
		initRight();
		break;
	case 3:
		initUp();
		break;
	case 4:
		initLeft();
		break;
	default:
		break;
	}
}

TriangleFigure::~TriangleFigure()
{
}
