#pragma once
class DrawCell
{
private:
	HANDLE handel = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD coord;
	int elementCode;
	int xConverted;
	int yConverted;
	void convertCoordinates(int x, int y);
	void drawCellByConvertCoordinates();
	void printTwoChars();
public:
	DrawCell();
	void drawByCoordinates(int x, int y);
	void clearByCoordinates(int x, int y);
	void writeStringByCoordinates(char *string, int x, int y);
	void clearPreviousFigurePosition(FigureCells **currentFigureCells);
	void drawFigure(FigureCells **currentFigureCells, int color);
	void increaseYCoordinates(FigureCells **currentFigureCells);
	void setColorOutputChar(int color);
	~DrawCell();
};

