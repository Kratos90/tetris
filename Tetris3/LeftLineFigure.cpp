#include "stdafx.h"
#include "LeftLineFigure.h"


LeftLineFigure::LeftLineFigure()
{
	color = 14; //yellow
	quantityPositions = 4;
	generatePosition();
	initFigureCoordinates();
}

void LeftLineFigure::initUp()
{
	if (x == 0 && y == 0){
		x = 4;
		y = 1;
	}else if (y == 0)
		y = 1;
	else if (isChangePosition)
		fromRightToUp();

	int i = 0, limit = x + 3;

	figureCoordinates[i]->x = x;
	figureCoordinates[i]->y = y - 1;

	for (i = 1; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void LeftLineFigure::initDown()
{
	if (x == 0 && y == 0)
		x = 4;
	else if (isChangePosition)
		fromLeftToDown();

	int i, limit = x + 3;

	for (i = 0; x < limit; x++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}

	figureCoordinates[i]->x = x - 1;
	figureCoordinates[i]->y = y + 1;
}

void LeftLineFigure::initLeft()
{
	if (x == 0 && y == 0)
		x = 5;
	else if (isChangePosition)
		fromUpToLeft();

	int i = 0, limit = y + 3;

	figureCoordinates[i]->x = x - 1;
	figureCoordinates[i]->y = y + 2;

	for (i = 1; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void LeftLineFigure::initRight()
{
	if (x == 0 && y == 0)
		x = 4;
	else if (isChangePosition)
		fromDownToRight();

	int i = 0, limit = y + 3;

	figureCoordinates[i]->x = x + 1;
	figureCoordinates[i]->y = y;

	for (i = 1; y < limit; y++, i++) {
		figureCoordinates[i]->x = x;
		figureCoordinates[i]->y = y;
	}
}

void LeftLineFigure::fromDownToRight()
{
	x++;
	y -= 2;
}

void LeftLineFigure::fromRightToUp()
{

}

void LeftLineFigure::fromUpToLeft()
{
	x++;
	y -= 2;
}

void LeftLineFigure::fromLeftToDown()
{
	y--;
}

void LeftLineFigure::initFigureCoordinates()
{
	switch (position)
	{
	case 1:
		initLeft();
		break;
	case 2:
		initDown();
		break;
	case 3:
		initRight();
		break;
	case 4:
		initUp();
		break;
	default:
		break;
	}
}

LeftLineFigure::~LeftLineFigure()
{
}

